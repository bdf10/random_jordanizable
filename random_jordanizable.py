from functools import partial
from sage.all import (
    Partitions,
    PositiveIntegers,
    ZZ,
    jordan_block,
    matrix,
    random_matrix
)


def random_jordanizable(e_vals, geo_mults=None, alg_mults=None, P_bound=5):
    if alg_mults is None:
        alg_mults = geo_mults
    if len(e_vals) != len(geo_mults):
        if len(e_vals) > len(geo_mults):
            raise ValueError('Not enough geometric multiplicities! '
                             'Each eigenvalue must be assigned to a '
                             'geometric multiplicity.')
        raise ValueError('Too many geometric multiplicities! '
                         'The number of geometric multiplicities should not '
                         'exceed the number of eigenvalues.')
    if len(e_vals) != len(alg_mults):
        if len(e_vals) > len(alg_mults):
            raise ValueError('Not enough algebraic multiplicities! '
                             'Each eigenvalue must be assigned to an '
                             'algebraic multiplicity.')
        raise ValueError('Too many algebraic multiplicities! '
                         'The number of algebraic multiplicities should not '
                         'exceed the number of eigenvalues.')
    if any(map(lambda x: x not in PositiveIntegers(), geo_mults)):
        raise ValueError('Each geometric multiplicity must be '
                         'a positive integer.')
    if any(map(lambda x: x not in PositiveIntegers(), alg_mults)):
        raise ValueError('Each algebraic multiplicity must be '
                         'a positive integer.')
    if any(map(lambda g, a: g > a, *zip(geo_mults, alg_mults))):
        raise ValueError("Each eigenvalue's geometric multiplicity cannot "
                         "exceed its algebraic multiplicity.")
    J = matrix([])
    for e_val, geo_mult, alg_mult in zip(e_vals, geo_mults, alg_mults):
        block_sizes = Partitions(alg_mult, length=geo_mult).random_element()
        blocks = map(partial(jordan_block, e_val), block_sizes)
        J = reduce(lambda X, Y: X.block_sum(Y), [J] + blocks)
    n = sum(alg_mults)
    P = random_matrix(ZZ, n, algorithm='unimodular', upper_bound=P_bound)
    return P*J*P.inverse()
